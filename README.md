# Simple Tennis Game #

This repository is for my implementation of basic tennis game application.
This application uses maven to run tests and prepare a build (executable jar package).

### How to run tests? ###
```
$mvn test
```

### How to make executable jar? ###
```
$mvn package
```

### How to run it? ###
```
$java -jar target/tennis-1.0-SNAPSHOT.jar <integer param>
```
Tennis application receives an integer parameter. It represents game "rounds". If this parameter is not passed, one round is set as the default.