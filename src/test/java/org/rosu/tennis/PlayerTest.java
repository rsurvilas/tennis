package org.rosu.tennis;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.stream.IntStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by rsurvilas on 11/5/16.
 *
 * Player test class.
 */
@RunWith(MockitoJUnitRunner.class)
public class PlayerTest {
    private static final String[] description = {"love", "fifteen", "thirty", "forty"};

    @Mock private Player foo;

    @Test
    public void testDescriptions() {
        IntStream.rangeClosed(0, 3).forEach(i -> testDescription(i));
    }

    private void testDescription(int i) {
        when(foo.getScore()).thenReturn(i);
        when(foo.getScoreDescription()).thenReturn(description[i]);
        assertEquals(foo.getScoreDescription(), description[i]);
    }

    @Test
    public void testWinBall() {
        when(foo.getScore()).thenReturn(4);

        IntStream.rangeClosed(1, 4).forEach((Integer) -> foo.winBall());

        assertEquals(foo.getScore(), 4);
    }
}