package org.rosu.tennis;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.stream.IntStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by rsurvilas on 11/5/16.
 *
 * Game test class.
 */
@RunWith(MockitoJUnitRunner.class)
public class GameTest {

    @Mock private Player foo;
    @Mock private Player bar;

    private Game game;

    @Before
    public void initGame() {
        when(foo.getName()).thenReturn("Foo");
        when(bar.getName()).thenReturn("Bar");
        this.game = new Game(this.foo, this.bar);
    }

    @Test
    public void getGameResults() {
        int min = 0;
        int max = 15;

        IntStream.rangeClosed(min, max).forEach(i ->
                IntStream.rangeClosed(min, max).forEach(j -> testGameResult(i, j)));
    }

    private void testGameResult(int i, int j) {
        when(foo.getScore()).thenReturn(i);
        when(foo.getName()).thenReturn("Foo");
        when(bar.getScore()).thenReturn(j);
        when(bar.getName()).thenReturn("Bar");

        switch (Math.abs(bar.getScore() - foo.getScore())) {
            case 0:
                assertEquals(game.getGameResult(), game.DEUCE);
                break;
            case 1:
                assertEquals(game.getGameResult(), game.ADVANTAGE + game.getLeadPlayerName());
                break;
            default:
                assertEquals(game.getGameResult(), game.getLeadPlayerName() + game.WON);
        }
    }

    @Test
    public void testLeadPlayerName() {
        // mocking player Foo:
        when(foo.getName()).thenReturn("Foo");
        when(foo.getScore()).thenReturn(10);

        // mocking player Bar:
        when(bar.getName()).thenReturn("Bar");
        when(bar.getScore()).thenReturn(5);

        // testing real getLeadPlayerName method:
        assertEquals(game.getLeadPlayerName(), "Foo");
    }
}