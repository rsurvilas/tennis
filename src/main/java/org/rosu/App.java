package org.rosu;

import org.rosu.tennis.Game;
import org.rosu.tennis.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;
import java.util.stream.IntStream;

/**
 * Created by rsurvilas on 11/5/16.
 *
 * Main application class.
 */
public class App {
    private static final Logger LOG = LoggerFactory.getLogger(App.class);
    private static final int MAGIC_SEED_NUMBER = 42;

    public static void main(String[] args) {
        int rounds = getRoundsFromArgs(args);

        Player foo = new Player("Foo");
        Player bar = new Player("Bar");

        Random random = new Random(MAGIC_SEED_NUMBER);

        IntStream.rangeClosed(1, rounds).forEach(i -> playGame(i, foo, bar, random));
    }

    private static void playGame(int gameNo, Player foo, Player bar, Random random) {
        LOG.info("Starting tennis game round {}.", gameNo);

        // resetting scores for new round:
        foo.resetScore();
        bar.resetScore();
        Game game = new Game(foo, bar);

        int fooRandomWinBall = getNonNegativeRandom(random);
        IntStream.rangeClosed(0, fooRandomWinBall).forEach(i -> foo.winBall());

        fooRandomWinBall = getNonNegativeRandom(random);
        IntStream.rangeClosed(0, fooRandomWinBall).forEach(i -> bar.winBall());

        LOG.info("Results for round {}: {}", gameNo, game.getScore());
    }

    private static int getRoundsFromArgs(String args[]) {
        int rounds = 1;

        if (args.length == 0) {
            LOG.error("Enter how many rounds would You like to play. Using default = 1.");
            return rounds;
        }

        try {
            rounds = Integer.valueOf(args[0]);
        } catch (NumberFormatException n) {
            LOG.error("This game accepts only integers: {}. Using default = 1.", n.getMessage());
        }

        return rounds;
    }

    /**
     * Generates non-negative random number.
     *
     * Its weird, but nextInt(bound) sometimes returns negative number... I'm
     * using Math.abs() to handle this case.
     *
     * @param random Random number generator.
     *
     * @return Non-negative random number.
     */
    private static int getNonNegativeRandom(Random random) {
        return Math.abs(random.nextInt(
            Math.abs(random.nextInt(Integer.SIZE - 1))));
    }
}
