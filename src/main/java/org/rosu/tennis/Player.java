package org.rosu.tennis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by rsurvilas on 11/5/16.
 *
 * Player class.
 */
public class Player {
    private static final Logger LOG = LoggerFactory.getLogger(Player.class);

    private static final String[] description = {"love", "fifteen", "thirty", "forty"};
    private String name;
    private int score = 0;

    public Player(String name) {
        LOG.info("Player {} was created.", name);

        this.name = name;
    }

    int getScore() {
        return score;
    }

    String getName() {
        return name;
    }

    public void winBall() {
        this.score++;

        LOG.info("Player {} wins a ball, his score is {}", name, score);
    }

    public void resetScore() {
        this.score = 0;
    }

    String getScoreDescription() {
        String scoreDescription = description[getScore()];

        LOG.info("Player's {} score is {}", name, scoreDescription);

        return scoreDescription;
    }
}
