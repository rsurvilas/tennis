package org.rosu.tennis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by rsurvilas on 11/5/16.
 *
 * Game class.
 */
public class Game {
    private static final Logger LOG = LoggerFactory.getLogger(Game.class);

    final static String WON = " won";
    final static String DEUCE = "deuce";
    final static String ADVANTAGE = "advantage ";

    private Player foo;
    private Player bar;

    public Game(Player foo, Player bar) {
        this.foo = foo;
        this.bar = bar;

        LOG.info("Players {} and {} are registered for tennis game!", foo.getName(), bar.getName());
    }

    /**
     * Gets the final score of the game.
     * */
    public String getScore() {
        switch (foo.getScore() + bar.getScore()) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                return foo.getScoreDescription() + ", " + bar.getScoreDescription();
            default:
                return getGameResult();
        }
    }

    /**
     * Finds out the result of the game.
     * */
    String getGameResult() {
        switch (Math.abs(bar.getScore() - foo.getScore())) {
            case 0:
                return DEUCE;
            case 1:
                return ADVANTAGE + getLeadPlayerName();
            default:
                return getLeadPlayerName() + WON;
        }
    }

    String getLeadPlayerName() {
        return (foo.getScore() > bar.getScore()) ? foo.getName() : bar.getName();
    }
}
